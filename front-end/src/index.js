import React from 'react';
import { render } from 'react-dom';

import Layout from './components/Layout';
import Header from './components/Header';
import './styles.css';

const app = () =>
document.getElementById('app')
  

render(<Layout />, app);
