import React from 'react';
import axios from 'axios';


export default class Button extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      status: "Order",
      value: this.props.ProductIdAliex.ProductIdAliexpress,
      products:[],
    };

    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    this.setState({value: event.target.value});
  }
  handleClick(e) {
    e.preventDefault();
    this.setState({ status: "Ordered" }) 
    axios
      .get(
        window.encodeURI(
          `https://db9b6c27.ngrok.io/product?product_id=`+this.state.value,
        ),
      ).then(response => {
        console.log(response);
      });
  }
render(){
    return (
    <div><input type="text" value={this.state.value} onChange={this.handleChange} />
     <button onClick={this.handleClick.bind(this)}>
    {this.state.status}{this.state.value}
      </button>
      {this.state.products}
      </div>
    );
  }


}
