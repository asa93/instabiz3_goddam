import React from 'react';
import axios from 'axios';
import Header from './Header';
import Button from './Button';
const Product = ({ product, index }) =>
  <tr>
    <td>{index + 1}</td>
    <td className="repo-name">{product.ProductName}</td>
    <td>{product.ProductIdShopify} ID Shopify</td>
    <Button ProductIdAliex={product}/>
  </tr>;

export default class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      loading: true,
      error: null,
      value: '',
    };
   
  }
 
  componentDidMount() {
    axios
      .get(
        window.encodeURI(
          `https://0d56ea11.ngrok.io/api/products`,
        ),
      )
      .then(response => {

        const products = response.data;
		  console.log(response);
        this.setState({
          products,
          loading: false,
        });
      })
      .catch(error => {
        this.setState({
          error: error,
          loading: false,
        });
      });
  }
  
  renderLoading() {
    return (
      <div>
        Loading...
      </div>
    );
  }

  renderError() {
    return (
      <div>
        <div>
          Sorry, an error ocurred: {this.state.error.response.data.message}
        </div>
      </div>
    );
  }

  renderList() {
    const products = this.state.products;
    const error=this.state.error;
console.log(products);
    if (error) {
      console.log(error);
      return this.renderError();
    }

    return (
	<div>
    <h1>{this.props.title}</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>#</th>
            <th>Product title</th>
            <th>Price</th>
            <th>Id Aliexpress</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product, index) =>
            <Product product={product} index={index} key={product.Id} />,
          )}
		  
        </tbody>
      </table>
	  <p>{JSON.stringify(this.state)}</p></div>
    );
  }

  render() {
    return this.state.loading ? this.renderLoading() : this.renderList();
  }
}
