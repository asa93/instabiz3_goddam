import React from 'react';
import axios from 'axios';


import Header from './Header';
import Footer from './Footer';
import Products from './Products';
export default class Layout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
      loading: true,
      error: null,
    };
  }

render(){
    return (<div>
      <Header />
      <Products title='Products' />
      <Footer />
      </div>
    );
  }


}
